import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', 
    [Validators.required,
    Validators.minLength(3),
    Validators.maxLength(15)]),

    password: new FormControl('', 
    [ Validators.required,
      Validators.minLength(3),
      Validators.maxLength(15)]),
  });

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  get username() {
    return this.loginForm.get('username');

  }

  onLoginClicked() {
    console.log(this.username.value);
    this.router.navigateByUrl(`/feed`);

  }

}
