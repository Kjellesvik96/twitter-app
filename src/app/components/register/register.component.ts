import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', 
    [Validators.required,
    Validators.minLength(3),
    Validators.maxLength(15)]),

    email: new FormControl('', 
    [Validators.required,
    Validators.minLength(3),
    Validators.maxLength(25)]),

    password: new FormControl('', 
    [ Validators.required,
      Validators.minLength(3),
      Validators.maxLength(10)]),
  });

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  getUserData() {
    return this.registerForm.get('username'),
           this.registerForm.get('email'),
           this.registerForm.get('password');
  }

  onRegisterClicked() {
    this.router.navigateByUrl(`/login`);
    this.getUserData();
  }

}
